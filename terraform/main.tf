terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
}

provider "azurerm" {
  features {}

  subscription_id   = "110007d5-f2fd-464d-a307-b1fcd22da360"
  tenant_id         = "9a5d51f0-4e44-4531-a345-679028d89bb2"
  client_id         = "26ed9ccd-ec70-4dcf-aadd-e425d90b3cbc"
  client_secret     = "VXhT6jyaS.56NPWiWyadyKB5_uVANzcueU"
}

resource "azurerm_resource_group" "example" {
  name     = "example-resources"
  location = "West US"
}

resource "azurerm_kubernetes_cluster" "example" {
  name                = "soabel-prueba-aks1"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  dns_prefix          = "soabel-prueba-aks1"

  default_node_pool {
    name       = "default"
    node_count = 2
    vm_size    = "Standard_D2_v2"
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    Environment = "Production"
  }
}

output "client_certificate" {
  value = azurerm_kubernetes_cluster.example.kube_config.0.client_certificate
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.example.kube_config_raw

  sensitive = true
}
